<?php include 'commons/header.php'; ?>

<div class="login-form">
    <h3>Login</h3>

	<form method="post" id="loginForm">
		<input type="hidden" value="loginRequest" name="postres">
		<div class="form-group">
			<label for="username">Username</label>
			<input type="text" class="form-control" id="username" aria-describedby="userHelp" placeholder="Enter Username" name="username">
			<small id="userHelp" class="form-text text-muted">We'll never share your username with anyone else.</small>
		</div>
		<div class="form-group">
			<label for="password">Password</label>
			<input type="password" class="form-control" id="password" placeholder="Password" name="password">
		</div>

		<button type="button" class="btn btn-primary" id="loginFormSubmit">Submit</button>
	</form>
</div> 
    
<div class="login-map">
    <h3>You are here:</h3>
    <iframe src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d15830.163355290879!2d125.66881984999999!3d7.2929647!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sph!4v1552372135812" 
    width="700" height="400" frameborder="0" style="border:0;" allowfullscreen ></iframe>
</div>

<?php include 'commons/footer.php'; ?>

<script>
	$(document).ready(function(){

		$('#loginFormSubmit').click(function(){
			var ito = $(this);
			var form = $("#loginForm");

			console.log(form.serialize());

			$.ajax({
		        type:"POST",
		        data:form.serialize(), 
		        dataType: "html",
		        async: false,
		        url:"core/functions.php",
		        success : function(data) {
		        	console.log(data);
		        },
		        error : function() {
		           console.log("Error");
		        }
		    });
		});

	});
</script>