<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?php echo isset($pageTitle) ? $pageTitle : "CamrRush: Website Application"; ?>
    </title>

    <link rel="icon" class="mobileview" href="assets/images/logo.png">
    <link rel="stylesheet" href="assets/css/main.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    
</head>

<body id="body">

    <div id="wrapper">

        <header>
            <div class="navbar navbar-inverse navbar-fixed-top">
                <div class="container">
                    <div class="navbar-header">
                        <a href="index.html">
                            <img src="assets/images/logo.png" style="height: 60px; top: 0px; margin: 10px;">
                        </a>
                        <a class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="glyphicon glyphicon-menu-hamburger" style="color: white;"></span>
                            <i class="fa fa-bars"></i>
                        </a>
                    </div>
                    <div class="navbar-collapse collapse">

                        <ul class="nav pull-right navbar-nav">
                            <ul class="nav navbar-nav">
                                <?php if(isset($auth) && $auth==true){ ?>
                                    <li class="navi"><a href="index.php">Home</a></li>
                                    <li class="navi"><a href="logs.php">Logs</a></li>
                                    <li class="navi"><a href="sos.php">SOS Calls</a></li>
                                    <li class="navi"><a href="unit.php">Profile</a></li>
                                <?php } else { ?>
                                    <li class="navi"><a href="login.php">Login</a></li>
                                    <li class="navi"><a href="signup.php">Sign Up</a></li>
                                <?php } ?>
                            </ul>
                        </ul>
                    </div>
                </div>
            </div>
        </header>


